# MySQL Regisztrációs rendszer v1.0

## Komponensek

 - BackgroundWorker *(+ Animáció a folyamatok alatt)*
 - Button
 - LinkLabel
 - Label
 - Panel
 - PictureBox *(GIF támogatással)*
 - Timer
 - TextBox
 
 
## Megvalósítások
 + Animáció
 + BCrypt titkosítás
 + Betűtípus váltás automatikusan [Open Sans]
 + Hiba log
 + MySQL
 + MySQL beállítása új formról [XML mentéssel] 
 + Egyéb form beállítások (szegélynélküli mozgatható, árnyék)


## **Adatbázis incizializálása:** 


```sql
 
CREATE DATABASE IF NOT EXISTS `db_sysauth` CHARACTER SET utf8 COLLATE utf8_hungarian_ci;

CREATE TABLE IF NOT EXISTS `users`(
	`id` int(11) not null auto_increment primary key,
	`email` varchar(255) not null,
	`username` varchar(255) not null,
	`password` varchar(255) not null,
	`password_salt` varchar(255) not null,
	`regDate` datetime not null
) CHARACTER SET utf8 COLLATE utf8_hungarian_ci;

```

--------
--------
**2020. 04. 17.**




 
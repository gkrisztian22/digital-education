﻿using MySql.Data.MySqlClient;

namespace Regrendszer
{
    static class Adatbazis
    {
        
        public static MySqlConnection sql;
        public static string[] db =
            {
                "127.0.0.1", // Host
                "root", // Username
                "", // Password
                "db_sysauth", // Database
                "None", // SslMode

            };

        public static void Connection_Setup()
        {
            sql = new MySqlConnection($"SERVER={db[0]};UID={db[1]};PASSWORD={db[2]};DATABASE={db[3]};SslMode={db[4]};Charset=utf8");
        }

        public static bool OpenSql()
        {
            try
            {
                Connection_Setup();
                sql.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                
                string msg = "";
                if (ex.Number == 0)
                    msg = "Nem sikerült kapcsolódni az adatbázishoz: a szerver nem elérhető.";
                else if (ex.Number == 1045) msg = "Nem sikerült kapcsolódni az adatbázishoz: helytelen adatok (felhasználó/jelszó/adatbázis).";
                else msg = "Nem sikerült kapcsolódni az adatbázishoz: " + ex.Message;
                F.Log("error.txt", msg);
                return false;
            }
        }

    }
}

﻿using System;
using MySql.Data.MySqlClient;


namespace Regrendszer
{
    static class Login
    {
        
        public static bool CheckUser(string username)
        {
            if (Adatbazis.OpenSql())
            {
                try
                {
                    string selectUser = "SELECT COUNT(*) FROM `users` WHERE `username` like @u";
                    MySqlCommand cmd = new MySqlCommand(selectUser, Adatbazis.sql);

                    cmd.Parameters.Add("@u", MySqlDbType.VarChar).Value = username;
                    cmd.Prepare();
                    int count = Convert.ToInt32(cmd.ExecuteScalar());
                    

                    return count > 0;



                }
                catch (Exception ex)
                {
                    F.Log("error.txt", "[CheckUser] " + ex.Message);
                    return false;
                }
                
            }
            else return false;
        }
        public static bool LogIn(string username, string password)
        {
            if (Adatbazis.OpenSql())
            {
                try
                {
                    //string passwdSalt = BCrypt.Net.BCrypt.GenerateSalt();
                    //string passwdHashed = BCrypt.Net.BCrypt.HashPassword(password,passwdSalt);
                    string selectUser = "SELECT `password` FROM `users` WHERE `username` like @u ;";

                    MySqlCommand cmd = new MySqlCommand(selectUser, Adatbazis.sql);
                    cmd.Parameters.AddWithValue("@u", username);
                    cmd.Prepare();

                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();
                        string hashedPasswd = reader.GetString(0);
                        Adatbazis.sql.Close();

                        return passwdMatch(hashedPasswd, password);


                    }
                    else return false;
                }
                catch (Exception ex)
                {
                    F.Log("error.txt", "[Login] " + ex.Message);
                    return false;
                }
            }
            else return false;
        }
        private static bool passwdMatch(string hashed, string passwd)
        {
            return BCrypt.Net.BCrypt.Verify(passwd, hashed);
        }
    }
}

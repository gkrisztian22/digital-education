﻿using System;
using MySql.Data.MySqlClient;

namespace Regrendszer
{

    class User
    {
        public string Email { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }

        public User(string[] data)
        {
            Email = data[0];
            Username = data[1];
            Password = data[2];
        }

        public bool Register()
        {
            if (Adatbazis.OpenSql())
            {
                try
                {
                    if (Login.CheckUser(this.Username))
                    {
                        return false;
                    }

                    string insertUser = "INSERT INTO `users`(id, email, username, password, password_salt, regDate) VALUES(not null, @e, @u, @p, @ps, now());";

                    using (MySqlCommand cmd = new MySqlCommand(insertUser, Adatbazis.sql))
                    {
                        cmd.Parameters.Add("@e", MySqlDbType.VarChar).Value = this.Email;
                        cmd.Parameters.Add("@u", MySqlDbType.VarChar).Value = this.Username;

                        string saltPasswd = BCrypt.Net.BCrypt.GenerateSalt();
                        string hashedPasswd = BCrypt.Net.BCrypt.HashPassword(this.Password);
                        cmd.Parameters.Add("@p", MySqlDbType.VarChar).Value = hashedPasswd;
                        cmd.Parameters.Add("@ps", MySqlDbType.VarChar).Value = saltPasswd;
                        Adatbazis.sql.Close();
                        return cmd.ExecuteNonQuery() > 0;


                    }


                }
                catch (Exception ex)
                {
                    F.Log("error.txt", "[Register] " + ex.Message);
                    return false;
                }
            }
            else return false;
        }

         
       

    }
}

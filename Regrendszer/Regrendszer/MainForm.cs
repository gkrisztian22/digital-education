﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Regrendszer
{
    public partial class MainForm : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        const int WS_MINIMIZEBOX = 0x20000;
        const int CS_DBLCLKS = 0x8;
        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x30000;
                CreateParams cp = base.CreateParams;
                cp.Style |= WS_MINIMIZEBOX; // Tálcán ikonra klikkelésre: Minimize
                cp.ClassStyle |= CS_DBLCLKS;
                cp.ClassStyle |= CS_DROPSHADOW; // Árnyék a formra
                return cp;
            }   
        }
        int pRegHeight = 0;
        GifImage gf;
        Timer tGif;
        bool pRegHidden = true;
        public MainForm()
        {
            InitializeComponent();
            
            pRegHeight = pReg.Height;
            pReg.Height = 0;
            pReg.Visible = false;


            ShowLoading();
            picLoader.Visible = false;
            

        }
        
        private void ShowLoading()
        {
            gf = new GifImage("Resources/l2.gif");
            gf.ReverseAtEnd = false;
            tGif = new Timer();

            tGif.Tick += TGif_Tick;
        }

        private void TGif_Tick(object sender, EventArgs e)
        {
            picLoader.Image = gf.GetNextFrame();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            switchFont(); // OpenSans font
        }
        private void switchFont()
        {
            PrivateFontCollection pfc = new PrivateFontCollection();
            pfc.AddFontFile("Resources/OpenSans-Regular.ttf");
            
            float tempSize = 0;
            foreach (var item in pReg.Controls)
            {
                if (item is Label)
                {
                    var i = item as Label;
                    tempSize = i.Font.Size;
                    i.Font = new Font(pfc.Families[0], tempSize, FontStyle.Regular);

                }
                else if(item is Button)
                {
                    var i = item as Button;
                    tempSize = i.Font.Size;
                    i.Font = new Font(pfc.Families[0], tempSize, FontStyle.Bold);
                }
                else if(item is TextBox)
                {
                    var i = item as TextBox;
                    tempSize = i.Font.Size;
                    i.Font = new Font(pfc.Families[0], tempSize, FontStyle.Regular);
                }
            }
            foreach (var item in pLogin.Controls)
            {
                if (item is Label)
                {
                    var i = item as Label;
                    tempSize = i.Font.Size;
                    i.Font = new Font(pfc.Families[0], tempSize, FontStyle.Regular);

                }
                else if (item is Button)
                {
                    var i = item as Button;
                    tempSize = i.Font.Size;
                    i.Font = new Font(pfc.Families[0], tempSize, FontStyle.Bold);
                }
                else if (item is TextBox)
                {
                    var i = item as TextBox;
                    tempSize = i.Font.Size;
                    i.Font = new Font(pfc.Families[0], tempSize, FontStyle.Regular);
                }
            }
            Refresh();
        }
        private void Frm_MouseDown(object sender, MouseEventArgs e) // Form mozgatása [komponensre kattintva nem működik]
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Btn_Login_Click(object sender, EventArgs e)
        {
            //Bejelentkezés
            if (!Check.sEmpty(txt_l_user.Text) && !Check.sEmpty(txt_l_passwd.Text))
            {
                BackgroundWorker bgw = new BackgroundWorker();

                picLoader.Visible = true;
                tGif.Start();
                bool[] login = new bool[2]; // [0] = CheckUser ? true : false   |  [1] = Login ? true : false
                bgw.DoWork += (_, __) =>
                {
                    login[0] = Login.CheckUser(txt_l_user.Text);
                    login[1] = Login.LogIn(txt_l_user.Text, txt_l_passwd.Text);
                };
                bgw.RunWorkerCompleted += (_, __) =>
                {
                    tGif.Stop();
                    picLoader.Visible = false;
                    if (!login[0])
                    {
                        MessageBox.Show("Nem létezik a felhasználó!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (login[1])
                        {
                            MessageBox.Show("Sikeres bejelentkezés!", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Helytelen adatokat adott meg!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                };
                bgw.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show("A mezők nem lehetnek üresek!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }
        
        private void Btn_Reg_Click(object sender, EventArgs e)
        {
            //Regisztráció
            if (txt_reg_passwd.Text != txt_reg_passwd2.Text && txt_reg_passwd.Text == "")
            {
                MessageBox.Show("A jelszó mezők nem lehetnek üresek, illetve egyezniük kell!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (!Check.sEmpty(txt_reg_email.Text) && !Check.sEmpty(txt_reg_user.Text))
                {
                    if (Check.isEmailFormat(txt_reg_email.Text))
                    {
                        BackgroundWorker bgw = new BackgroundWorker();
                        picLoader.Visible = true;
                        tGif.Start();
                        bool ok = false;

                        bgw.DoWork += (_, __) =>
                        {
                            string[] d = { txt_reg_email.Text, txt_reg_user.Text, txt_reg_passwd.Text };
                            User u = new User(d);

                            ok = u.Register();

                        };
                        bgw.RunWorkerCompleted += (_, __) =>
                        {
                            tGif.Stop();
                            picLoader.Visible = false;
                            if (ok)
                            {
                                MessageBox.Show("Sikeres regisztráció!", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Nem sikerült a regisztráció!" + Environment.NewLine + "Lehetséges, hogy már létezik ilyen felhasználó.", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        };
                        bgw.RunWorkerAsync();
                    }
                    else
                    {
                        MessageBox.Show("Hibás email formátum.", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                }
                else
                {
                    MessageBox.Show("A mezők nem lehetnek üresek!", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            

        }

        private void Lnk_pLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            timer1.Start();
        }
        
        private void Timer1_Tick(object sender, EventArgs e)
        {
            // Panel animáció
            if (pRegHidden)
            {
                pReg.Height += 10;
                if (pReg.Height >= pRegHeight)
                {
                    pRegHidden = false;
                    timer1.Stop();
                    this.Refresh();
                }
            }
            else
            {
                pReg.Height -= 10;
                if (pReg.Height <= 0)
                {
                    pRegHidden = true;
                    timer1.Stop();
                    this.Refresh();
                }
            }
            
        }

        private void Lnk_pReg_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pReg.Visible = true;
            timer1.Start();  
        }

        private void PictureBox4_Click(object sender, EventArgs e)
        {
            Beallitasok b = new Beallitasok();
            b.Show();
            
        }

        private void Label9_Click(object sender, EventArgs e)
        {
            PictureBox4_Click(sender, e);
        }

        private void Label10_Click(object sender, EventArgs e)
        {
            Process.Start("error.txt");
        }

        private void PictureBox5_Click(object sender, EventArgs e)
        {
            Label10_Click(sender, e);
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnMinim_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}

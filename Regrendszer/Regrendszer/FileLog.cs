﻿using System;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Regrendszer
{
    public static class F
    {
        public static void Log(string file, string msg)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(file, true, Encoding.UTF8))
                {
                    sw.WriteLine($"[ {DateTime.Now} ] - {msg}");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Hiba", "Nem sikerült a fájlba írás." + Environment.NewLine + ex.Message, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
    }
}

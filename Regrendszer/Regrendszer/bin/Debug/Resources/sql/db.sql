CREATE DATABASE IF NOT EXISTS `db_sysauth` CHARACTER SET utf8 COLLATE utf8_hungarian_ci;

CREATE TABLE IF NOT EXISTS `users`(
	`id` int(11) not null auto_increment primary key,
	`email` varchar(255) not null,
	`username` varchar(255) not null,
	`password` varchar(255) not null,
	`password_salt` varchar(255) not null,
	`regDate` datetime not null
) CHARACTER SET utf8 COLLATE utf8_hungarian_ci;
﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace Regrendszer
{
    public partial class Beallitasok : Form
    {
        public Beallitasok()
        {
            InitializeComponent();

            for (int i = 0; i < 5; i++)
            {
                Adatbazis.db[i] = readXml()[i];
            }
            

            textBox1.Text = Adatbazis.db[0];
            textBox2.Text = Adatbazis.db[1];
            textBox3.Text = Adatbazis.db[2];
            textBox4.Text = Adatbazis.db[3];
            checkBox1.Checked = (Adatbazis.db[4] == "None" ? false : true);
        }
        string[] readXml()
        {
            string[] adatok = new string[5];

            XmlDocument xd = new XmlDocument();
            xd.Load("save_db.xml");

            XmlElement xe = xd.DocumentElement;

            adatok[0] = xe.SelectSingleNode("kiszolgalo").InnerText;
            adatok[1] = xe.SelectSingleNode("felhasznalo").InnerText;
            adatok[2] = xe.SelectSingleNode("jelszo").InnerText;
            adatok[3] = xe.SelectSingleNode("adatbazis").InnerText;
            adatok[4] = xe.SelectSingleNode("ssl").InnerText;

            
            return adatok;
        }
        void saveXml()
        {
            XmlDocument xd = new XmlDocument();
            xd.Load("save_db.xml");
            XmlElement xe = xd.DocumentElement;

            xe.SelectSingleNode("kiszolgalo").InnerText = Adatbazis.db[0];
            xe.SelectSingleNode("felhasznalo").InnerText = Adatbazis.db[1];
            xe.SelectSingleNode("jelszo").InnerText = Adatbazis.db[2];
            xe.SelectSingleNode("adatbazis").InnerText = Adatbazis.db[3];
            xe.SelectSingleNode("ssl").InnerText = Adatbazis.db[4];

            xd.Save("save_db.xml");
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            Adatbazis.db[0] = textBox1.Text;
            Adatbazis.db[1] = textBox2.Text;
            Adatbazis.db[2] = textBox3.Text;
            Adatbazis.db[3] = textBox4.Text;
            Adatbazis.db[4] = (checkBox1.Checked ? "" : "None");
            saveXml();

            this.Close();
        }
    }
}

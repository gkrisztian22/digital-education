﻿namespace Regrendszer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_l_user = new System.Windows.Forms.TextBox();
            this.txt_l_passwd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Login = new System.Windows.Forms.Button();
            this.pLogin = new System.Windows.Forms.Panel();
            this.lnk_pReg = new System.Windows.Forms.LinkLabel();
            this.pReg = new System.Windows.Forms.Panel();
            this.txt_reg_passwd2 = new System.Windows.Forms.TextBox();
            this.txt_reg_passwd = new System.Windows.Forms.TextBox();
            this.txt_reg_user = new System.Windows.Forms.TextBox();
            this.txt_reg_email = new System.Windows.Forms.TextBox();
            this.lnk_pLogin = new System.Windows.Forms.LinkLabel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_Reg = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMinim = new System.Windows.Forms.Button();
            this.picLoader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pLogin.SuspendLayout();
            this.pReg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(153, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(87, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(96, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Bejelentkezés";
            // 
            // txt_l_user
            // 
            this.txt_l_user.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txt_l_user.Location = new System.Drawing.Point(64, 96);
            this.txt_l_user.Name = "txt_l_user";
            this.txt_l_user.Size = new System.Drawing.Size(216, 24);
            this.txt_l_user.TabIndex = 2;
            // 
            // txt_l_passwd
            // 
            this.txt_l_passwd.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txt_l_passwd.Location = new System.Drawing.Point(64, 144);
            this.txt_l_passwd.Name = "txt_l_passwd";
            this.txt_l_passwd.Size = new System.Drawing.Size(216, 24);
            this.txt_l_passwd.TabIndex = 3;
            this.txt_l_passwd.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(62, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Felhasználónév";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(62, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Jelszó";
            // 
            // btn_Login
            // 
            this.btn_Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(65)))), ((int)(((byte)(250)))));
            this.btn_Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Login.Font = new System.Drawing.Font("Microsoft YaHei UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Login.ForeColor = System.Drawing.Color.White;
            this.btn_Login.Location = new System.Drawing.Point(64, 192);
            this.btn_Login.Name = "btn_Login";
            this.btn_Login.Size = new System.Drawing.Size(216, 32);
            this.btn_Login.TabIndex = 6;
            this.btn_Login.Text = "Bejelentkezés";
            this.btn_Login.UseVisualStyleBackColor = false;
            this.btn_Login.Click += new System.EventHandler(this.Btn_Login_Click);
            // 
            // pLogin
            // 
            this.pLogin.Controls.Add(this.lnk_pReg);
            this.pLogin.Controls.Add(this.btn_Login);
            this.pLogin.Controls.Add(this.label3);
            this.pLogin.Controls.Add(this.label2);
            this.pLogin.Controls.Add(this.txt_l_passwd);
            this.pLogin.Controls.Add(this.txt_l_user);
            this.pLogin.Controls.Add(this.label1);
            this.pLogin.Location = new System.Drawing.Point(33, 136);
            this.pLogin.Name = "pLogin";
            this.pLogin.Size = new System.Drawing.Size(352, 280);
            this.pLogin.TabIndex = 7;
            // 
            // lnk_pReg
            // 
            this.lnk_pReg.AutoSize = true;
            this.lnk_pReg.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnk_pReg.Location = new System.Drawing.Point(200, 240);
            this.lnk_pReg.Name = "lnk_pReg";
            this.lnk_pReg.Size = new System.Drawing.Size(79, 13);
            this.lnk_pReg.TabIndex = 16;
            this.lnk_pReg.TabStop = true;
            this.lnk_pReg.Text = "Regisztráció →";
            this.lnk_pReg.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Lnk_pReg_LinkClicked);
            // 
            // pReg
            // 
            this.pReg.Controls.Add(this.txt_reg_passwd2);
            this.pReg.Controls.Add(this.txt_reg_passwd);
            this.pReg.Controls.Add(this.txt_reg_user);
            this.pReg.Controls.Add(this.txt_reg_email);
            this.pReg.Controls.Add(this.lnk_pLogin);
            this.pReg.Controls.Add(this.label8);
            this.pReg.Controls.Add(this.label7);
            this.pReg.Controls.Add(this.btn_Reg);
            this.pReg.Controls.Add(this.label4);
            this.pReg.Controls.Add(this.label5);
            this.pReg.Controls.Add(this.label6);
            this.pReg.Location = new System.Drawing.Point(32, 136);
            this.pReg.Name = "pReg";
            this.pReg.Size = new System.Drawing.Size(352, 336);
            this.pReg.TabIndex = 8;
            // 
            // txt_reg_passwd2
            // 
            this.txt_reg_passwd2.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F);
            this.txt_reg_passwd2.Location = new System.Drawing.Point(64, 216);
            this.txt_reg_passwd2.Name = "txt_reg_passwd2";
            this.txt_reg_passwd2.Size = new System.Drawing.Size(216, 24);
            this.txt_reg_passwd2.TabIndex = 15;
            this.txt_reg_passwd2.UseSystemPasswordChar = true;
            // 
            // txt_reg_passwd
            // 
            this.txt_reg_passwd.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F);
            this.txt_reg_passwd.Location = new System.Drawing.Point(64, 168);
            this.txt_reg_passwd.Name = "txt_reg_passwd";
            this.txt_reg_passwd.Size = new System.Drawing.Size(216, 24);
            this.txt_reg_passwd.TabIndex = 14;
            this.txt_reg_passwd.UseSystemPasswordChar = true;
            // 
            // txt_reg_user
            // 
            this.txt_reg_user.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F);
            this.txt_reg_user.Location = new System.Drawing.Point(64, 120);
            this.txt_reg_user.Name = "txt_reg_user";
            this.txt_reg_user.Size = new System.Drawing.Size(216, 24);
            this.txt_reg_user.TabIndex = 13;
            // 
            // txt_reg_email
            // 
            this.txt_reg_email.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F);
            this.txt_reg_email.Location = new System.Drawing.Point(64, 72);
            this.txt_reg_email.Name = "txt_reg_email";
            this.txt_reg_email.Size = new System.Drawing.Size(216, 24);
            this.txt_reg_email.TabIndex = 12;
            // 
            // lnk_pLogin
            // 
            this.lnk_pLogin.AutoSize = true;
            this.lnk_pLogin.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnk_pLogin.Location = new System.Drawing.Point(192, 296);
            this.lnk_pLogin.Name = "lnk_pLogin";
            this.lnk_pLogin.Size = new System.Drawing.Size(87, 13);
            this.lnk_pLogin.TabIndex = 11;
            this.lnk_pLogin.TabStop = true;
            this.lnk_pLogin.Text = "Bejelentkezés →";
            this.lnk_pLogin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Lnk_pLogin_LinkClicked);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(62, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "Jelszó újra";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(62, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "Email cím";
            // 
            // btn_Reg
            // 
            this.btn_Reg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(65)))), ((int)(((byte)(250)))));
            this.btn_Reg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Reg.Font = new System.Drawing.Font("Microsoft YaHei UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Reg.ForeColor = System.Drawing.Color.White;
            this.btn_Reg.Location = new System.Drawing.Point(64, 248);
            this.btn_Reg.Name = "btn_Reg";
            this.btn_Reg.Size = new System.Drawing.Size(216, 32);
            this.btn_Reg.TabIndex = 6;
            this.btn_Reg.Text = "Regisztráció";
            this.btn_Reg.UseVisualStyleBackColor = false;
            this.btn_Reg.Click += new System.EventHandler(this.Btn_Reg_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(62, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Jelszó";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(62, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Felhasználónév";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(103, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 26);
            this.label6.TabIndex = 1;
            this.label6.Text = "Regisztráció";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(321, 502);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(19, 17);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.PictureBox4_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(338, 502);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 17);
            this.label9.TabIndex = 16;
            this.label9.Text = "Beállítások";
            this.label9.Click += new System.EventHandler(this.Label9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(269, 502);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 17);
            this.label10.TabIndex = 18;
            this.label10.Text = "Hibák";
            this.label10.Click += new System.EventHandler(this.Label10_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(252, 502);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(19, 17);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 17;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.PictureBox5_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(382, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(36, 25);
            this.btnClose.TabIndex = 19;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnMinim
            // 
            this.btnMinim.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMinim.BackgroundImage")));
            this.btnMinim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnMinim.FlatAppearance.BorderSize = 0;
            this.btnMinim.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinim.Location = new System.Drawing.Point(363, 0);
            this.btnMinim.Name = "btnMinim";
            this.btnMinim.Size = new System.Drawing.Size(21, 25);
            this.btnMinim.TabIndex = 20;
            this.btnMinim.UseVisualStyleBackColor = true;
            this.btnMinim.Click += new System.EventHandler(this.BtnMinim_Click);
            // 
            // picLoader
            // 
            this.picLoader.Location = new System.Drawing.Point(104, 26);
            this.picLoader.Name = "picLoader";
            this.picLoader.Size = new System.Drawing.Size(203, 102);
            this.picLoader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoader.TabIndex = 21;
            this.picLoader.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(415, 531);
            this.Controls.Add(this.picLoader);
            this.Controls.Add(this.btnMinim);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pReg);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Opacity = 0.95D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Regrendszer v1.0";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pLogin.ResumeLayout(false);
            this.pLogin.PerformLayout();
            this.pReg.ResumeLayout(false);
            this.pReg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_l_user;
        private System.Windows.Forms.TextBox txt_l_passwd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Login;
        private System.Windows.Forms.Panel pLogin;
        private System.Windows.Forms.Panel pReg;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_Reg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel lnk_pLogin;
        private System.Windows.Forms.TextBox txt_reg_passwd2;
        private System.Windows.Forms.TextBox txt_reg_passwd;
        private System.Windows.Forms.TextBox txt_reg_user;
        private System.Windows.Forms.TextBox txt_reg_email;
        private System.Windows.Forms.LinkLabel lnk_pReg;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMinim;
        private System.Windows.Forms.PictureBox picLoader;
    }
}


﻿using System;
using System.Linq;

namespace Regrendszer
{
    static class Check
    {
        public static bool isEmailFormat(string email)
        {
            return email.Contains('@');
        }
        public static bool sEmpty(string s)
        {
            return string.IsNullOrEmpty(s) || string.IsNullOrWhiteSpace(s);
        }
    }
}

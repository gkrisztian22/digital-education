using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Alapműveletes
{
    

    public partial class Form1 : Form
    {
        public Művelet m;
        public Random r = new Random();
        public List<string> megoldások = new List<string>(); // Eddigi megoldások 
        
        public void feladat()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "0";
            label1.Text = "";

            m = new Művelet();
            m.A = r.Next(0, 100);
            m.B = r.Next(0, 100);

            int x = r.Next(0, 4);
            label1.Text = m.MűveletiJel[x].ToString();
            m.Megoldás(m.MűveletiJel[x]);
            textBox1.Text = m.A.ToString();
            textBox2.Text = m.B.ToString();

            //Aktuális feladat hozzáadása a listához
            megoldások.Add(string.Format("{0} {1} {2} = {3}", m.A.ToString(), m.MűveletiJel[x].ToString(), m.B.ToString(), m.Megoldás(m.MűveletiJel[x]).ToString()));
        }

        public Form1()
        {
            InitializeComponent();
            feladat();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            m.Eredmény = Convert.ToInt32(textBox3.Text);
            m.Kiír();
            if (m.mivan == DialogResult.Yes) feladat();
            else
            {
                //ListBox-ba töltés: 1. megoldás
                foreach (var item in megoldások)
                    listBox1.Items.Add(item);

                //ListBox-ba töltés: 2. megoldás
                //listBox1.DataSource = megoldások;
            }

        }
    }
    public class Művelet
    {
        public char[] MűveletiJel = {'+','-','*','/'};
        public DialogResult mivan;
        private int a;
        public int A
        {
            get { return a; }
            set { a = value; }
        }
        private int b;
        public int B
        {
            get { return b; }
            set { b = value; }
        }
        private int eredmény;
        public int Eredmény
        {
            get { return eredmény; }
            set { eredmény = value; }
        }
        private int megoldás;

        public int Megoldás(char c)
        {
            this.megoldás = 0;
            switch (c)
            {
                case '+': this.megoldás = this.a + this.b; break;
                case '-': this.megoldás = this.a - this.b; break;
                case '*': this.megoldás = this.a * this.b; break;
                case '/': this.megoldás = this.a / this.b; break;
            }
            return this.megoldás;
        }

        private string üzenet;

        public void Kiír()
        {
            if (Jó()) üzenet = "Rendben";
            else üzenet = String.Format(" nem Ok, a helyes megoldás: {0}\nÚj feladat?",this.megoldás);
            if (MessageBox.Show(üzenet, "Értékelés", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.mivan = DialogResult.Yes;
            else
                this.mivan = DialogResult.No;
        }

        public bool Jó()
        {
            return megoldás == eredmény;
        }

    }

      //public class Összeadás : Művelet
      //{
      //    public int Megoldás
      //    {
      //        set { megoldás = A + B; }
      //    }
      //}

      //public class Kivonás : Művelet
      //{
      //    public int Megoldás
      //    {
      //        set { megoldás = A - B; }
      //    }
      //}

      //public class Szorzás : Művelet
      //{
      //    public int Megoldás
      //    {
      //        set { megoldás = A * B; }
      //    }
      //}

      //public class Osztás : Művelet
      //{
      //    public int Megoldás
      //    {
      //        set { megoldás = A / B; }
      //    }
      //}
}
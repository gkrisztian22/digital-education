﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lift
{
    class Program
    {
        struct Adat
        {
            public DateTime időpont { get; private set; }
            public int kártya_id { get; private set; }
            public int induló_szint { get; private set; }
            public int cél_szint { get; private set; }

            public Adat(string str)
            {
                string[] s = str.Split(' ');
                this.időpont = DateTime.Parse(s[0]);
                this.kártya_id = int.Parse(s[1]);
                this.induló_szint = int.Parse(s[2]);
                this.cél_szint = int.Parse(s[3]);
            }
        }
        static List<Adat> l = new List<Adat>();
        static bool isNumber(string s)
        {
            try
            {
                int.Parse(s);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        static void Main(string[] args)
        {
            Read();
            Console.WriteLine("3. feladat: Összes lift használat: " + l.Count);
            Console.WriteLine("4. feladat: Időszak: {0} - {1}", l.OrderBy(x => x.időpont).First().időpont.ToShortDateString(), l.OrderBy(x => x.időpont).Last().időpont.ToShortDateString()) ;
            Console.WriteLine("5. feladat: Célszint max: " + l.Max(x=>x.cél_szint));

            Console.WriteLine("6. feladat: ");
            string kartya_sz, celszint;
            Console.Write("\tKártya száma: ");
            kartya_sz = Console.ReadLine();
            Console.Write("\tCélszint száma: ");
            celszint = Console.ReadLine();

            int kartya, szint;
            if (isNumber(kartya_sz))
                kartya = int.Parse(kartya_sz);
            else kartya = 5;
            if (isNumber(celszint))
                szint = int.Parse(celszint);
            else szint = 5;

            Console.WriteLine("7. feladat: A(z) {0} kártyával{1} utaztak a(z) {2}. emeletre!", kartya, (l.Count(x=>x.kártya_id == kartya && x.cél_szint == szint) > 0 ? "": " nem"), szint);


            Console.WriteLine("8. feladat: Statisztika");
            var stat = l.GroupBy(x => x.időpont);
            foreach (var item in stat)
            {
                Console.WriteLine($"\t{item.Key.Year}.{item.Key.Month.ToString("00")}.{item.Key.Day.ToString("00")} - {l.Count(x=>x.időpont == item.Key)}x");
            }


            Console.ReadKey();
        }
        static void Read()
        {
            try
            {
                using (StreamReader sr = new StreamReader("lift.txt", Encoding.UTF8))
                {
                    while (!sr.EndOfStream)
                    {
                        l.Add(new Adat(sr.ReadLine()));
                    }
                }
            }
            catch (FileNotFoundException)
            {

                Console.WriteLine("A fájl nem található.");
            }
        }
    }
}
